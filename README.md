# Runner

This project contains the asynchronous retrospectives of the Runner group.

The team as used by the bot that creates issues is defined at
https://gitlab.com/gitlab-org/async-retrospectives/blob/master/teams.yml